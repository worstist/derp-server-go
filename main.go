package main

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/worstist/derp-server-go/proxy"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func newClientHandler(p proxy.Proxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		user, _, _ := r.BasicAuth()
		agentId := proxy.AgentId(user)
		log.WithField("agentId", agentId).Info("Handling client request.")
		proxyRequest, err := proxy.HttpRequestToProxyRequest(r)
		if err != nil {
			log.WithError(err).Error("While trying to create proxy request.")
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		response, err := p.ForwardRequest(agentId, proxyRequest)
		if err != nil {
			log.WithError(err).Error("While trying to forward request.")
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		if response == nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("Server did not return response."))
			return
		}
		log.WithField("agentId", agentId).Info("Returning response from agent.")
		w.WriteHeader(response.StatusCode)
		for k, v := range response.Headers {
			w.Header().Set(k, v)
		}
		_, err = w.Write(response.Body)
		if err != nil {
			log.WithError(err).Error("While writing response to client.")
		}
	}
}

func newPollHandler(p proxy.Proxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		agentId := proxy.AgentId(vars["agentId"])
		log.WithFields(log.Fields{
			"agentId": agentId,
		}).Info("Handling poll request.")
		proxyResponse, err := proxy.HttpRequestToProxyResponse(r)
		if err != nil {
			log.WithError(err).Error("While trying to create proxy response.")
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		err = p.Respond(agentId, proxyResponse)
		if err != nil {
			log.WithFields(log.Fields{
				"agentId":   agentId,
				"requestId": vars["requestId"],
			}).WithError(err).Error("While responding.")
		}

		log.WithField("agentId", agentId).Info("Agent subscribing for request.")
		request, err := p.Subscribe(agentId)
		if err != nil {
			if err == proxy.SubscribeTimeoutError {
				log.WithField("agentId", agentId).Info("Timed out waiting for request.")
				w.WriteHeader(http.StatusRequestTimeout)
				return
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write([]byte(err.Error()))
				return
			}
		}
		jsonBytes, err := json.Marshal(request)
		if err != nil {
			log.WithError(err).Error("Could not json encode proxied request.")
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte(err.Error()))
			return
		}
		w.Header().Set("Content-Type", "application/json")
		_, err = w.Write(jsonBytes)
		if err != nil {
			log.WithError(err).Error("While writing response (client request) to agent.")
		}
	}
}

func main() {
	p := proxy.NewAgentsProxy()

	r := mux.NewRouter()
	pollHandler := newPollHandler(p)
	r.HandleFunc("/poll/{agentId}", pollHandler)
	r.PathPrefix("/").HandlerFunc(newClientHandler(p))
	http.Handle("/", r)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
