package proxy

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

type AgentId string

type Request struct {
	Body          []byte            `json:"body"`
	Headers       map[string]string `json:"headers"`
	Id            string            `json:"id"`
	Method        string            `json:"method"`
	RemoteAddress string            `json:"remoteAddress"`
	Uri           string            `json:"uri"`
}

type Response struct {
	Body       []byte            `json:"body"`
	Headers    map[string]string `json:"headers"`
	Id         string            `json:"id"`
	StatusCode int               `json:"statusCode"`
}

func headers(h http.Header) map[string]string {
	headers := make(map[string]string)
	for k := range h {
		headers[k] = h.Get(k)
	}
	return headers
}

func HttpRequestToProxyRequest(r *http.Request) (*Request, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	splitRemoteAddr := strings.SplitN(r.RemoteAddr, ":", 2)
	return &Request{
		body,
		headers(r.Header),
		newRequestId(),
		r.Method,
		splitRemoteAddr[0],
		r.RequestURI,
	}, nil
}

func HttpRequestToProxyResponse(r *http.Request) (*Response, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	if len(body) == 0 {
		return nil, nil
	}
	var response Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}
	return &response, nil
}

type Proxy interface {
	ForwardRequest(agentId AgentId, request *Request) (*Response, error)
	Subscribe(agentId AgentId) (*Request, error)
	Respond(agentId AgentId, response *Response) error
}
