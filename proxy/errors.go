package proxy

import "errors"

var AgentAlreadyRegisteredError = errors.New("agent already registered")
var AgentUnavailableError = errors.New("agent unavailable")
var ResponseTimeoutError = errors.New("agent response timed out")
var SubscribeTimeoutError = errors.New("subscription timed out")