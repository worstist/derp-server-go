module bitbucket.org/worstist/derp-server-go

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.0
	github.com/sirupsen/logrus v1.4.0
)
