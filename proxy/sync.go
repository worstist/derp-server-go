package proxy

type syncAction interface {
	execute(agents agentsMap)
}

func (p *agentsProxy) synchronizer() {
	for {
		action := <-p.actions
		action.execute(p.agents)
	}
}

/* get */

type getAction struct {
	agentId AgentId
	reply   chan *agent
	err     chan error
}

func (action *getAction) execute(agents agentsMap) {
	agent, ok := agents[action.agentId]
	if ok {
		delete(agents, action.agentId)
		action.reply <- agent
	} else {
		action.err <- AgentUnavailableError
	}
	close(action.reply)
	close(action.err)
}

func newGetAction(agentId AgentId) *getAction {
	return &getAction{
		agentId,
		make(chan *agent),
		make(chan error),
	}
}

/* register */

type registerAction struct {
	agent *agent
	err   chan error
}

func (action *registerAction) execute(agents agentsMap) {
	_, ok := agents[action.agent.id]
	if ok {
		action.err <- AgentAlreadyRegisteredError
	} else {
		agents[action.agent.id] = action.agent
		action.err <- nil
	}
	close(action.err)
}

func newRegisterAction(agent *agent) *registerAction {
	return &registerAction{
		agent,
		make(chan error),
	}
}

/* unregister */

type unregisterAction struct {
	agentId AgentId
	err     chan error
}

func (action *unregisterAction) execute(agents agentsMap) {
	delete(agents, action.agentId)
	action.err <- nil
}

func newUnregisterAction(agentId AgentId) *unregisterAction {
	return &unregisterAction{
		agentId,
		make(chan error, 1),
	}
}
