package proxy

import (
	"errors"
	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)

func newRequestId() string {
	requestId, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}
	return requestId.String()
}

type agent struct {
	id                 AgentId
	forwardRequest     chan *Request
	respond            chan *Response
	waitingOnRequestId string
}

type agentsMap map[AgentId]*agent

type agentsProxy struct {
	actions chan syncAction
	agents  agentsMap
}

func (p agentsProxy) get(agentId AgentId) (*agent, error) {
	action := newGetAction(agentId)
	p.actions <- action
	select {
	case agent := <-action.reply:
		return agent, nil
	case err := <-action.err:
		// TODO: should probably wait a bit here and check again (possibly repeatedly)
		return nil, err
	}
}

func (p agentsProxy) register(agent *agent) error {
	action := newRegisterAction(agent)
	p.actions <- action
	err := <-action.err
	return err
}

func (p agentsProxy) unregister(agentId AgentId) error {
	action := newUnregisterAction(agentId)
	p.actions <- action
	err := <-action.err
	return err
}

func newRequestAgent(id AgentId) *agent {
	return &agent{
		id,
		make(chan *Request, 1),
		nil,
		"",
	}
}

func newResponseAgent(id AgentId, requestId string) *agent {
	return &agent{
		id,
		nil,
		make(chan *Response, 1),
		requestId,
	}
}

func timeout(seconds int) chan bool {
	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(time.Duration(seconds) * time.Second)
		timeout <- true
	}()
	return timeout
}

func (p agentsProxy) timeoutRespond(agent *agent) (*Response, error) {
	if agent.respond == nil {
		panic(errors.New("cannot wait on response without response channel"))
	}
	select {
	case request := <-agent.respond:
		return request, nil
	case <-timeout(30):
		// need to clean up the agent or it can't be registered again
		_ = p.unregister(agent.id)
		return nil, ResponseTimeoutError
	}
}

func (p agentsProxy) timeoutSubscribe(agent *agent) (*Request, error) {
	if agent.forwardRequest == nil {
		panic(errors.New("cannot forward request without forward request channel"))
	}
	select {
	case request := <-agent.forwardRequest:
		return request, nil
	case <-timeout(30):
		// need to clean up the agent or it can't be registered again
		_ = p.unregister(agent.id)
		return nil, SubscribeTimeoutError
	}
}

func (p agentsProxy) ForwardRequest(agentId AgentId, request *Request) (*Response, error) {
	log.WithField("agentId", agentId).Info("Forwarding request.")
	// get agent
	requestAgent, err := p.get(agentId)
	if err != nil {
		log.WithField("agentId", agentId).WithError(err).Info("Could not get agent to forward request.")
		return nil, err
	}
	// forward request to agent
	requestAgent.forwardRequest <- request
	close(requestAgent.forwardRequest)
	// register agent for response
	responseAgent := newResponseAgent(agentId, request.Id)
	err = p.register(responseAgent)
	if err != nil {
		log.WithField("agentId", agentId).WithError(err).Info("Could not register agent for response.")
		// try to put the request agent back
		_ = p.register(requestAgent)
		return nil, err
	}
	// wait for response from agent
	return p.timeoutRespond(responseAgent)
}

func (p agentsProxy) Subscribe(agentId AgentId) (*Request, error) {
	log.WithField("agentId", agentId).Info("Subscribing agent.")
	agent := newRequestAgent(agentId)
	err := p.register(agent)
	if err != nil {
		log.WithField("agentId", agentId).WithError(err).Info("Could not register agent for request.")
		return nil, err
	}
	log.WithField("agentId", agentId).Info("Agent listening for request.")
	return p.timeoutSubscribe(agent)
}

func (p agentsProxy) Respond(agentId AgentId, response *Response) error {
	if response == nil {
		return nil
	}
	log.WithFields(log.Fields{
		"agentId":   agentId,
		"requestId": response.Id,
	}).Info("Trying to respond to client request.")
	agent, err := p.get(agentId)
	if err != nil {
		log.WithField("agentId", agentId).WithError(err).Info("Could not get agent to respond to.")
		return err
	}
	if agent.respond == nil {
		return errors.New("cannot respond on nil response channel")
	}
	log.WithFields(log.Fields{
		"agentId":   agentId,
		"requestId": response.Id,
	}).Info("Responding to client request.")
	if agent.waitingOnRequestId == response.Id {
		agent.respond <- response
	} else {
		log.WithFields(log.Fields{
			"agentId":   agentId,
			"requestId": response.Id,
		}).Warn("Client is waiting on different request id. Sending nil.")
		agent.respond <- nil
	}
	close(agent.respond)
	return nil
}

func NewAgentsProxy() Proxy {
	actions := make(chan syncAction)
	p := agentsProxy{
		actions,
		make(agentsMap),
	}
	go p.synchronizer()
	return p
}
